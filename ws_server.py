import asyncio
import websockets
import json

conn_dict = {}
host_name = "localhost"
port_number = 1234


async def response(websocket, path):
    while True:
        data = await websocket.recv()
        print(data)
        data = json.loads(data)
        if not "key" in data:
            key = f'{data["cc"]}{data["mn"]}'
            try:
                if not key in conn_dict:
                    conn_dict[key] = websocket
            except websockets.ConnectionClosed:
                del conn_dict[key]
        elif "key" in data:
            try:
                key = data["key"]
                msg = data["msg"]
                await conn_dict[key].send(msg)
                print("Message sent to ", key)
            except:
                del conn_dict[key]
                pass


try:
    start_server = websockets.serve(response, host_name, port_number)
    asyncio.get_event_loop().run_until_complete(start_server)
    print(f"Server started at {host_name} at port {port_number}")
    asyncio.get_event_loop().run_forever()
except:
    pass
